package cn.iocoder.yudao.module.member.controller.app.auth.vo;

import cn.iocoder.yudao.framework.common.util.json.JsonUtils;
import lombok.Data;

@Data
public class YunmasResponse {

    private MsgGroup Message;

    @Data
    public static class MsgGroup{
        private String msgGroup;
        private String rspcod;
        private Boolean success;
    }

    public static void main(String[] args) {
        YunmasResponse yunmasResponse = new YunmasResponse();
        MsgGroup msgGroup = new YunmasResponse.MsgGroup();
        yunmasResponse.setMessage(msgGroup);


        String jsonString = JsonUtils.toJsonString(yunmasResponse);
        System.out.println("jsonString = " + jsonString);
    }
}
